#!/bin/bash

# check number of arguments
if [ $# -eq 0 ] ; then
    echo "no file path"
    exit 0
fi

# replace
replace_path=`echo $@ | sed 's/\\\ / /g'`

# open
cmd.exe /c start "" "$replace_path"

exit 0
